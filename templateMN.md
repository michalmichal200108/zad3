---
author: Michał Karczewski
title: Filipiny
subtitle: 
date: 
theme: Warsaw
output: beamer_presentation
header-includes: 
    \usepackage{xcolor}
    \usepackage{listings}
---



## Położenie

kraj leży w Azji Południowo-Wschodniej, w zachodniej część Oceanu Spokojnego, pomiędzy Chinami a Archipelagiem Indonezyjskim. Wyspy oblewają wody: Morza Celebes od południa, Morza Sulu od południowego zachodu, Morza Południowochińskiego od zachodu i północy, natomiast od wschodu znajdują się otwarte wody Oceanu Spokojnego.

**Powierzchnia**

*300000 kilometrów kwadratowych*
<!--- Kolorowanie tekstu wymaga użycia komend Latexa -->
\textcolor{red}{Powierzchnia podobna do Polski}
<!--- Centrowanie wymaga użycia komend Latexa -->



## Granice

Lista numerowana:
<!--- UWAGA WAŻNE pusta linia odstępu -->

1. Indonezja - *południe*
2. Malezja - *południowy zachód*
3. Wietnam - *zachód*
4. Chiny - *północ*



## Fauna

### Zwierzęta
* Delfiny
* Tygrysy
* Papugi


## Przykładowe potrawy

\begin{alertblock}{Kurczak Adobo}
Kurczak marynowany w occie
\end{alertblock}



\begin{block}{Kare Kare}
Gulasz z gęstym, pikantnym sosem orzechowym
\end{block}

\begin{exampleblock}{Balut}
Gotowane kacze jajko z zarodkiem w środku
![](pics/BALUT.jpg){ height=50% width=50%}
\end{exampleblock}

## Koszty wycieczki

1. Lot - od 2300 zł w obie strony za dwie osoby (zależy od miasta)
2. Hotel - najlepsze hotele od 200 zł do 1000 zł za dobę, pokój można znaleźć już za 20 zł za dobę (za dwie osoby)
3. Transport - wynajem skutera to 25 zł dziennie
4. Jedzenie - można zjeść już za 3 - 8 zł w tanich knajpach (jeden posiłek), w resortach jest to do 25 zł za posiłek
5. Woda - przeważnie ok 4 zł za butelkę
6. Alkohol - od 3 do 9 zł na lokalnych imprezach
7. Papierosy ok 5 zł